import { Id, Image, Tag, Rectangle, RectangleConfig, SocketMessage } from "./models"

const API_ROOT = "http://localhost:9090";
const WS_ROOT = "ws://localhost:9090/ws";

export async function getImages(): Promise<Image[]> {
    const res = await fetch(`${API_ROOT}/images`);
    const json = await res.json();
    return json as Image[];
}

export async function getTags(): Promise<Tag[]> {
    const res = await fetch(`${API_ROOT}/tags`);
    const json = await res.json();
    return json as Tag[];
}

export async function getImageIds(): Promise<number[]> {
    const res = await fetch(`${API_ROOT}/images/ids`);
    const json = await res.json();
    return json as number[];
}

export async function getImage(id: number, stumble: boolean = false): Promise<Image> {
    const res = await fetch(`${API_ROOT}/image/${id}${stumble ? "?stumble=true" : ""}`);
    if (!res.ok) {
        throw new Error("Bad HTTP response code.")
    }
    const json = await res.json();
    return json as Image;
}

export async function getRectangle(imageId: number, rectId: number): Promise<Rectangle> {
    const res = await fetch(`${API_ROOT}/image/${imageId}/rectangle/${rectId}`);
    const json = await res.json();
    return json as Rectangle;
}

export async function createRectangle(imageId: number, rect: RectangleConfig): Promise<Id> {
    const res = await fetch(`${API_ROOT}/image/${imageId}/rectangle`,
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(rect)
        });
    const json = await res.json();
    return json as Id;
}

export async function createImage(name: string): Promise<Id> {
    const res = await fetch(`${API_ROOT}/images/create`,
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ name: name })
        });
    const json = await res.json();
    return json as Id;
}

export async function deleteRectangle(imageId: number, rect: Id): Promise<string> {
    const res = await fetch(`${API_ROOT}/image/${imageId}/rectangle`,
        {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(rect)
        });
    return await res.text();
}

export async function setupSocket(msgCallback: (data: SocketMessage) => any): Promise<void> {
    return new Promise((resolve) => {
        const socket = new WebSocket(WS_ROOT);
        socket.addEventListener("close", () => setupSocket(msgCallback));
        socket.addEventListener("error", () => setupSocket(msgCallback));
        socket.addEventListener("open", () => {
            socket.addEventListener("message", (e) => msgCallback(JSON.parse(e.data)));
            resolve();
        });
    })
}
