export interface Id {
    id: number;
}

export interface Name {
    name: string;
}

export interface Image extends Name, Id {
    tags: Tag[];
    rectangles: Rectangle[];
}

export interface Tag extends Name, Id {
    imageCount: number;
}

export interface RectangleConfig {
    x: number;
    y: number;
    width: number;
    height: number;
    color: string;
}

export interface Rectangle extends RectangleConfig, Id {
    image: number;
}

interface ImageMessage {
    tag: "imageCreate";
    id: number;
}

interface RectangleMessage {
    tag: "rectangleCreate" | "rectangleDelete";
    id: number;
    imageId: number;
}

export type SocketMessage =
    | ImageMessage
    | RectangleMessage;
