# whiteboard viewer ts + vue + vuetify frontend

Assignment for [web applications mimuw course](https://kciebiera.github.io/www-2324/).

You can find the backend [here](https://gitlab.com/barteloniu/awww7).
There's also a [vanilla TS frontend](https://gitlab.com/barteloniu/awww8), where you can actually draw the images.

# how to run

```bash
npm install
npm run dev
```
